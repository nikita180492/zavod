/* JQuery hack for search any register character */
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function (arg) {
  return function (elem) {
    return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
  };
});
/**/

/* JQuery hack for position character in input */
jQuery.fn.setCursorPosition = function (pos) {
  if (jQuery(this).get(0).setSelectionRange) {
    jQuery(this).get(0).setSelectionRange(pos, pos);
  } else if (jQuery(this).get(0).createTextRange) {
    var range = jQuery(this).get(0).createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
};
/**/

$(document).ready(function () {

  /* Global custom select*/
  $(".select").select2();
  /**/

  /* Range filter elems */
  $("[data-range]").each(function (index, elem) {
    let obj = $(elem),
      step = obj.data('step'),
      minValue = obj.data('range-min'),
      maxValue = obj.data('range-max'),
      parent = obj.parents('[data-parent]').first(),
      inputFrom = parent.find('[data-range-from]').first(),
      inputTo = parent.find('[data-range-to]').first(),
      fromValue = (inputFrom.val()) ? inputFrom.val() : minValue,
      toValue = (inputTo.val()) ? inputTo.val() : maxValue;

    obj.slider({
      min: minValue,
      max: maxValue,
      step: step,
      animate: "slow",
      range: true,
      values: [fromValue, toValue],
      slide: function (event, ui) {
        inputFrom.val(ui.values[0]);
        inputTo.val(ui.values[1]);

        inputTo.trigger('keyup');
      }
    });

    inputFrom.val(obj.slider("values", 0));
    inputTo.val(obj.slider("values", 1));
  });
  /**/

  /* Autocomplete input */
  $('[data-autocomplete]').each(function (index, elem) {
    let obj = $(elem);

    if (obj.next().is('[data-autocomplete-options]')) {

      let options = obj.next();

      obj.on('focus', function (e) {

        var term = $.trim($(this).val());

        options.addClass('show');


        options.children('li').each(function (index, option) {
          $(option).on('click', function (e) {
            // e.stopPropagation();
            var text = $.trim($(this).text());
            obj.val(text);
            options.removeClass('show');
          });

        });
        options.removeClass('show');
        obj.trigger('input');

      });

      obj.on('input', function (e) {
        var term = $.trim($(this).val());

        if (term.length > 0) {
          options.children().hide();
          var optionsMatch = options.find(':Contains("' + term + '")');

          if (optionsMatch.length > 0) {
            optionsMatch.show();
            options.addClass('show');
          } else {
            options.removeClass('show');
          }
        }
      });
    }
  });
  /**/

  /* Phone masked*/
  $('[data-phone-mask]').click(function () {
    var index = $(this).val().indexOf('_');
    if (index > -1)
      $(this).setCursorPosition(index);
  }).mask("+7 999 999 99 99", {autoclear: false});
  /**/

  // $('.fa-search').click(function(e){
  //   e.preventDefault()
  //   e.stopPropagation()
  //   $(this).css({"display": "none"})
  //   $(".fa-close").css({"display": "block"})
  //   $('.header-search').slideToggle()
  // })

  const changeIconSearch = (e, el1, el2) => {
    e.preventDefault()
    e.stopPropagation()
    $(el1).css({"display": "none"})
    $(el2).css({"display": "block"})
    if ($('[data-name = "header-search"]').hasClass('open')) {
      $('[data-name = "header-search"]').removeClass('open')
    } else {
      $('[data-name = "header-search"]').addClass('open')
    }
  }

  $('.fa-search').click(e => changeIconSearch(e, '.fa-search', '.fa-close'))
  $('.fa-close').click(e => changeIconSearch(e, '.fa-close', '.fa-search'))

});
