$(document).ready(() => {
  $(".slider").slick(
    {
      dots: false,
      arrows: false,
      adaptiveHeight: false,
      slidesToShow: 5,
      slidesToScroll: 5,
      speed: 1000,
      easing: "ease",
      infinite: true,
      autoplay: false,
      autoplaySpeed: 1000,
      centerMode: false,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]
    }
  );

  $(".specification-slider").slick(
    {
      dots: false,
      // speed: 1500,
      slidesToShow: 3,
      easing: "ease",
      variableWidth: true,
      centerMode: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            centerMode: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            centerMode: true,
          }
        }
      ]
    }
  );
  $(".slider-one-slide").slick(
    {
      dots: false,
      infinite: true,
      speed: 1500,
      slidesToShow: 1,
      adaptiveHeight: true,
      easing: "ease",
      autoplay: true,
      autoplaySpeed: 1500
    }
  );
  const sliderSynck =()=>{
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav',
      variableWidth: false,
    });
    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      infinite: true
    });
  }
  sliderSynck()
  // $(document).resize(() => {
  //   sliderSynck()
  // })
  $(".slider-reviews").slick(
    {
      dots: false,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      variableWidth: false,
      centerMode: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true
          }
        }
      ]
    }
  );
  $(".selection-parts-slider").slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 1000,
    speed: 1000,
  })
});

